@echo off
if not exist "%JAVA_HOME%\bin\java.exe" echo Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 or later is better! & EXIT /B 1
set "JAVA=%JAVA_HOME%\bin\java.exe"

setlocal enabledelayedexpansion

set BASE_DIR=%~dp0
rem added double quotation marks to avoid the issue caused by the folder names containing spaces.
rem removed the last 5 chars(which means \bin\) to get the base DIR.
set BASE_DIR="%BASE_DIR:~0,-5%"

set CUSTOM_SEARCH_LOCATIONS=file:%BASE_DIR%/conf/

set SERVER=hom-agent
set SERVER_INDEX=-1


set i=0
for %%a in (%*) do (
    if "%%a" == "-s" ( set /a SERVER_INDEX=!i!+1 )
    set /a i+=1
)

set i=0
for %%a in (%*) do (
    if %SERVER_INDEX% == !i! (set SERVER="%%a")
    set /a i+=1
)


echo "hom agent is starting"
set "HOM_JVM_OPTS=-Xms512m -Xmx512m -Xmn256m"

rem set hom options
set "HOM_OPTS=%HOM_OPTS% -Dhom.home=%BASE_DIR% -Duser.timezone=GMT+08"
set "HOM_OPTS=%HOM_OPTS% -jar %BASE_DIR%\target\%SERVER%.jar"

rem set hom spring config location
set "HOM_CONFIG_OPTS=--spring.config.additional-location=%CUSTOM_SEARCH_LOCATIONS%"

rem set hom log4j file location
set "HOM_LOG4J_OPTS=--logging.config=%BASE_DIR%/conf/hom-logback.xml"


set COMMAND="%JAVA%" %HOM_JVM_OPTS% %HOM_OPTS% %HOM_CONFIG_OPTS% %HOM_LOG4J_OPTS% hom.hom %*

rem start hom command
%COMMAND%
