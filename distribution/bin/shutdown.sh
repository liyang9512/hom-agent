#!/bin/bash

cd `dirname $0`/../target
target_dir=`pwd`

pid=`ps ax | grep -i 'hom.hom' | grep ${target_dir} | grep java | grep -v grep | awk '{print $1}'`
if [ -z "$pid" ] ; then
        echo "No HomAgent running."
        exit -1;
fi

echo "The HomAgent(${pid}) is running..."

kill ${pid}

echo "Send shutdown request to HomAgent(${pid}) OK"
