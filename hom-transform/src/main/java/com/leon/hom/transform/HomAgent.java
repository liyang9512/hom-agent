package com.leon.hom.transform;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * http over mqtt agent
 */
@SpringBootApplication
public class HomAgent implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(HomAgent.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Thread.currentThread().join();
    }
}
