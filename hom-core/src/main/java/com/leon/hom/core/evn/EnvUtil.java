package com.leon.hom.core.evn;

import cn.hutool.core.util.StrUtil;

import java.nio.file.Paths;

public class EnvUtil {

    private static final String HOM_HOME = "hom.home";

    private static final String CONG_PATH = "conf";

    private static final String CONG_FILE = "hom.conf";

    private static String homHome;

    public static String homHome() {
        if (StrUtil.isBlank(homHome)) {
            homHome = System.getProperty(HOM_HOME);
        }
        return homHome;
    }

    public static String getConfFilePath() {
        return Paths.get(homHome(), CONG_PATH, CONG_FILE).toString();
    }

}
