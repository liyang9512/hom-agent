
package com.leon.hom.core.common;

/**
 * Constants.
 *
 * @author Leon
 */
public class Constants {

    public static final String NULL = "";

    public static final String HOM = "hom";

    public static final String CONTENT_TYPE = "Content-Type";

}
