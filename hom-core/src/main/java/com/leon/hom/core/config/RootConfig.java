package com.leon.hom.core.config;

import java.util.List;

public class RootConfig {

    private String tag;

    private String redirect;

    private BrokerConfig broker;

    private List<ListenConfig> listen;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public BrokerConfig getBroker() {
        return broker;
    }

    public void setBroker(BrokerConfig broker) {
        this.broker = broker;
    }

    public List<ListenConfig> getListen() {
        return listen;
    }

    public void setListen(List<ListenConfig> listen) {
        this.listen = listen;
    }
}
