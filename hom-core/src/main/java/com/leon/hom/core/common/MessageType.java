package com.leon.hom.core.common;

public enum MessageType {

    REQUEST("request"),
    RESPONSE("response");

    String type;

    MessageType(String type) {
        this.type = type;
    }


    public String getType() {
        return type;
    }
}
