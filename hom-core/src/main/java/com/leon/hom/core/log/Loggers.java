

package com.leon.hom.core.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loggers for core.
 *
 * @author Leon
 */
public class Loggers {

    public static final Logger CORE = LoggerFactory.getLogger("com.leon.hom.core");

    public static final Logger HTTP = LoggerFactory.getLogger("com.leon.hom.http");

    public static final Logger MQTT = LoggerFactory.getLogger("com.leon.hom.mqtt");

    public static final Logger TRANSFORM = LoggerFactory.getLogger("com.leon.hom.transform");

}
