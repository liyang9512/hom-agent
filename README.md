# Hom Agent
<a href='https://gitee.com/liyang9512/hom-agent/stargazers'><img src='https://gitee.com/liyang9512/hom-agent/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/liyang9512/hom-agent/stargazers'><img src='https://gitee.com/liyang9512/hom-agent/badge/star.svg?theme=dark' alt='star'></img></a>

### 一、简介
Hom意为HTTP OVER MQTT，基于MQTT实现了HTTP的通讯方式，让两个存在MQTT通路的服务，通过MQTT实现HTTP交互。
![](img/HomAgent.png)
### 二、使用场景
物联网云边协同 \
基于边缘端与云端的MQTT通路，在不打通新的端口的情况使用本代理，实现云端与边缘端HTTP交互，可用于云端调用边缘端API接口等情况。

### 三、打包&安装

#### 源码打包

```bash
git clone https://gitee.com/liyang9512/hom-agent.git
cd hom-agent
mvn -Prelease-hom -Dmaven.test.skip=true clean install -U
```

#### 安装

```bash
unzip hom-agent-version.zip 或者 tar -xvf hom-agent-version.tar.gz
cd hom/bin
```

### 三、启动&停止

```bash
cd hom/bin

#windows start
startup.cmd

#linux start
sh startup.sh

#windows shutdown
shutdown.cmd

#linux shutdown
sh shutdown.sh
```


### 四、配置说明
````
# 本代理标识
tag = "agent0"

# 本地请求重定向
redirect = "http://127.0.0.1"

# mqtt连接配置
broker = {
    url = "tcp://127.0.0.1:1883"
    username = "test"
    password = "test"
    qos = 1
}

# http监听配置
listen = [
    {
        port = 9000
        target = 9099
        tag = "agent1"
    }
]
````
tag为本代理标识，在同一个mqtt broker下不可重复。 \
redirect用于配置本代理发出的http请求重定向地址，默认请求本地:http://127.0.0.1。 \
broker用于配置本代理连接mqtt broker的相关信息。  \
listen用于配置本代理要监听的http端口，以及目标代理的请求端口(target)和标识(tag)。