package com.leon.hom.mqtt.callback;

import org.eclipse.paho.client.mqttv3.MqttMessage;

@FunctionalInterface
public interface ReceiveMqttCallback {

    void handle(String topic, MqttMessage mqttMessage);

}
