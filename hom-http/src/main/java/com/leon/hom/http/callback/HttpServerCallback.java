package com.leon.hom.http.callback;

import com.leon.hom.http.entity.HttpRequestParam;

@FunctionalInterface
public interface HttpServerCallback {

    void handle(HttpRequestParam requestParam);

}
